import React from 'react';

const UserContext = React.createContext()

// use to allow other components to use the context object and supply necessary information needed to the context object
export const UserProvider = UserContext.Provider

export default UserContext;