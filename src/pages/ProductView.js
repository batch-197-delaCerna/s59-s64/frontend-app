import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	// useContext hook for global state
	const user = useContext(UserContext)

	const navigate = useNavigate(); //useHistory

	// The "useParams" hook allows us to retrieve the productId passed via URL
	const { productId, orderId } = useParams();

	// const [image, setImage] = useState("")
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


	const items = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/orders`, {
			method: "POST",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: 1
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data === true) {
				Swal.fire({
				title: "Successfully added",
				icon: "success",
				text: "You have successfully added the item."
			})

			navigate("/products");

		} else {

				Swal.fire({
				title: "Login to Purchase",
				icon: "error",
				text: "Please try again."
			})
			navigate("/login");
		}
		
	})

};


	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			// console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			// setImage(data.image);
		})

	}, [productId])

	return (
		<Container>
			<Row>
			  <Col lg={{span:6, offset:3}}>
			    <Card className="productCard my-3">
					{/* <Card.Img variant="top" src={image} /> */}
					<Card.Header className="productName">{name}</Card.Header>
					<Card.Body>
                		<Card.Text className="productDescription">{description}</Card.Text>
					</Card.Body>
					<Card.Footer className="footer">
					<Card.Text>Price: P{price}</Card.Text>
               		</Card.Footer>
					   <Card.Footer className="footer">
			          <div className="d-grip gap-2">
			          {
			          		(user.id !== null) ?
			          			<Button className="bg-primary" onClick={() => items(productId)}>Add</Button>
			          			:
			          			<Button className="bg-primary" as={Link} to="/login">Log in to Add!</Button>
			          }
			          </div>
					  </Card.Footer>
			      </Card>
			  </Col>
			</Row>
		</Container>
	)

}
