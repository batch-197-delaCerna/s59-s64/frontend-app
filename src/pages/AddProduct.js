import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'

export default function AddProduct(){
    const {user} = useContext(UserContext);

    const navigate = useNavigate()

    // const [image, setImage] = useState('')
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0)

    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        if(name !== "" && description !== "" && price > 0 
        //&& image !== ""
        ){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [name, description, price, 
        //image
        ])

    function addProduct(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
            })
        })
        .then (res => res.json())
        .then (data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: "Product added successfully!",
                    icon: "success",
                    text: `${name} is now added`
                })
                navigate("/admin")
            }else{
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `Something went wrong. Please try again!`
                })
            navigate('/admin')
            }
        })
         setName('');
         setDescription('');
         setPrice(0)
    }

    return(
        user.isAdmin
        ?
            <>
            <h1 className="my-5 text-center header">Add Product</h1>
            <Form onSubmit={(e) => addProduct(e)}>

                    <Form.Group controlId="name" className="mb-3">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Enter Product Name" 
                            value = {name}
                            onChange={e => setName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="description" className="mb-3">
                        <Form.Label>Product Description</Form.Label>
                        <Form.Control
                            as="textarea"
                            rows={3}
                            placeholder="Enter Product Description" 
                            value = {description}
                            onChange={e => setDescription(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="price" className="mb-3">
                        <Form.Label>Product Price</Form.Label>
                        <Form.Control 
                            type="number" 
                            placeholder="Enter Product Price" 
                            value = {price}
                            onChange={e => setPrice(e.target.value)}
                            required
                        />
                    </Form.Group>
                  
                    { isActive 
                        ? 
                        <Button variant="primary" type="submit" id="submitBtn">
                            Save
                        </Button>
                        : 
                        <Button variant="secondary" type="submit" id="submitBtn" disabled>
                            Save
                        </Button>
                    }
                        <Button className="m-2" as={Link} to="/admin" variant="success" type="submit" id="submitBtn">
                            Cancel
                        </Button>
                 </Form>
            </>
    :
        <Navigate to="/products"/>
    )
}
