import React from "react";
import "./Landing.css";
import Slideshow from "./Slideshow";

const collection = [
  {
    src:
      "https://wallpaperaccess.com/full/1431892.jpg"
  },
  {
    src:
      "https://wallpaperaccess.com/full/1076712.jpg"
  },
  {
    src:
      "https://wallpaperaccess.com/full/2546175.jpg"
  },
  {
    src: 
    "https://wallpaperaccess.com/full/1076701.jpg"
  },
  {
    src: 
    "https://wallpaperaccess.com/full/6369939.jpg"
  }
];

function Body() {
  return (
    <div className="body">
      <div className="bodyContent">
        <h1>d'Cafe</h1>
        <h5>
        Coffee that fuels your dreams.
        </h5>
        <br />
      </div>
      {/* <CarouselPage /> */}
      <div className="slideshowStyles">
        <Slideshow
          input={collection}
          ratio={`3:2`}
          mode={`automatic`}
          timeout={`2000`}
        />
      </div>
    </div>
  );
}

export default Body;
