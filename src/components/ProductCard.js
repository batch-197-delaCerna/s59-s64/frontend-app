import { useState, useEffect } from 'react';
import { Container,Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}){

    const { name, description, price, _id } = productProp;

    // state hook to store the state of items available
    const [count, setCount] = useState(0);
    const [items, setItems] = useState(30);

    function purchase(){
      if (items > 0 ) {
        setCount(count + 1)
        console.log('Items: ' + count)
        setItems(items - 1)
        console.log('Items: ' + items)
      } 
    };

    useEffect(() => {
        if (items === 0) {
            alert("Product it not available.")
        }
    }, [items])

  return(
    <>
    <Container>
      <Row>
        <Col lg={{span:6, offset:3}} className="cols">
          <Card className="my-3">
            <Card.Header className="productName">{name}</Card.Header>
              <Card.Body>
                <Card.Text className="productDescription">{description}</Card.Text>
              </Card.Body>
                <Card.Footer className="footer">
                  <Button className="bg-primary productButton mr-3" as={Link} to={`/products/${_id}`} >Details</Button>
                </Card.Footer>
            </Card>

        </Col>
      </Row>
    </Container>
    </>
  )
}
