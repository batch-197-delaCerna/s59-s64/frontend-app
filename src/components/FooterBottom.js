import React from "react";
import "./Footerbottom.css";

function FooterBottom() {
  return (
    <div className="footer">
      <div className="footerCards"><br/><br/><br/><br/>
      </div>
      <br/><br/><br/>

      <hr className="hrTag" pt-5/>
      <br />
      <div className="text-center">
      <a className="text-white mx-2" href="#">
        <i className="fa-brands fa-facebook fa-icons"></i>
      </a>
      <a className="text-white mx-2" href="#">
        <i className="fa-brands fa-instagram-square fa-icons"></i>
      </a>
      <a className="text-white mx-2" href="#">
        <i className="fa-brands fa-youtube-square fa-icons"></i>
      </a>
      <a className="text-white mx-2" href="#">
        <i className="fa-brands fa-tiktok fa-icons"></i>
      </a>
      <a className="text-white mx-2" href="#">
        <i className="fa-brands fa-linkedin fa-icons"></i>
      </a>
      <p>Copyright © 2022</p>
      <p>All rights reserved</p>
    </div>
    </div>);
    }

export default FooterBottom;