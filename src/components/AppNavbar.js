import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

    const { user } = useContext(UserContext)
	// console.log(user)
    
    return (
        <Navbar sticky="top" expand="lg" className="mr-5 pl-5">
		    <Container fluid>
            <Navbar.Brand as={Link} to="/" className="title">d'Cafe</Navbar.Brand>
		      <Navbar.Toggle aria-controls="navbarScroll" />
		      <Navbar.Collapse id="navbarScroll">
		        <Nav className="ms-auto navHead">
					{
						(user.isAdmin)
						?
						<Nav.Link as={Link} to="/admin">Admin Dashboard</Nav.Link>
						:
						<>
							<Nav.Link as={Link} to="/products">Products</Nav.Link>
{/*							<Nav.Link as={Link} to="/orders">Orders</Nav.Link>*/}
						</>
					}
		          	{ (user.id !== null) ?
						<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						:
						<>
						<Nav.Link as={Link} to="/login">Login</Nav.Link>
						<Nav.Link as={Link} to="/register">Register</Nav.Link>
						</>
				  	} 
		        </Nav>
		      </Navbar.Collapse>
		    </Container>
		</Navbar>
    )
}